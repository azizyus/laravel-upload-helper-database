<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 3/5/19
 * Time: 6:10 PM
 */

namespace Azizyus\UploadHelperDatabase;

use Illuminate\Support\ServiceProvider;

class LaravelUploadHelperDatabaseServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->loadMigrationsFrom(__DIR__."/Migrations");
        $this->mergeConfigFrom(__DIR__."/Config/general-images.php","general-images");
        $this->mergeConfigFrom(__DIR__."/Config/upload-helper-database.php","upload-helper-database");


        $this->publishes([

            __DIR__."/Config/general-images.php" => config_path("general-images.php"),
            __DIR__."/Config/upload-helper-database.php" => config_path("upload-helper-database.php")

        ],"azizyus/laravel-upload-helper-database-publish-config");

    }


    public function register()
    {


    }


}