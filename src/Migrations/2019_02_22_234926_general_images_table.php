<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("general_images",function (Blueprint $table){

            $table->increments("id");
            $table->text("fileName")->nullable();
            $table->unsignedInteger("tableEnum");
            $table->unsignedInteger("purposeEnum");
            $table->unsignedInteger("modelEnum");

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("general_images");
    }
}
