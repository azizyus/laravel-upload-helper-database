<?php


namespace Azizyus\UploadHelperDatabase\Hacks;


use Azizyus\UploadHelperDatabase\Models\GeneralImage;
use Illuminate\Support\Str;

trait CallMethod
{

    public function __call($method, $parameters)
    {


        foreach (config("general-images") as $item)
        {
            if(in_array($item["enum"],$this->imagePurposeEnums))
            {

                foreach ($item["variations"] as $variation)
                {

                    $relationName = $variation["relationName"];
                    $getMethodName = "get".Str::ucfirst($relationName);
                    $getBaseMethodName = $getMethodName."Base";
                    $getWithUploadDirectoryMethodName = $getMethodName."WithUploadDirectory";
                    if($relationName === $method)
                    {
                        return $this->hasOne(config("upload-helper-database.mainModel"),"modelEnum",$this->id)
                            ->where("tableEnum",$this->tableEnum())
                            ->where("purposeEnum", $variation["purposeEnum"]);
                    }
                    elseif($getMethodName === $method)
                    {
                        if($this->{$relationName})
                        {
                            return uploadHelperAsset($this->{$relationName}->fileName);
                        }
                        return null;
                    }
                    elseif($getBaseMethodName === $method)
                    {
                        if($this->{$relationName})
                        {
                            return ($this->{$relationName}->fileName);
                        }
                        return null;
                    }
                    elseif($getWithUploadDirectoryMethodName === $method)
                    {
                        if($this->{$relationName})
                        {
                            return wrapWithUploadDirectory(($this->{$relationName}->fileName));
                        }
                        return null;
                    }
                }

            }
        }

        return parent::__call($method, $parameters);
    }

}
