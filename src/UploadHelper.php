<?php



function uploadHelperAsset($asset)
{

    $dirName = config("upload-helper-database.uploadDirName");

    return asset("$dirName/$asset");

}

function wrapWithUploadDirectory($fileName)
{
    $dirName = config("upload-helper-database.uploadDirName");
    return "$dirName/$fileName";
}
