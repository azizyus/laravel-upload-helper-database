<?php


namespace Azizyus\UploadHelperDatabase\Helpers\Ammunitions;


use Azizyus\UploadHelperDatabase\Helpers\Interfaces\IAmmunition;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use LaravelUploadHelper\UploadHelper\UploadHelperFactory;

abstract class BaseAmmunition implements IAmmunition
{

    protected $uploadHelperFactory;
    public function __construct()
    {
        $this->uploadHelperFactory = new UploadHelperFactory();
    }

    public function setUploadHelperFactory(UploadHelperFactory $f)
    {
        $this->uploadHelperFactory = $f;
    }






}
