<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/23/19
 * Time: 12:08 AM
 */

namespace Azizyus\UploadHelperDatabase\Helpers\Ammunitions;


use Azizyus\UploadHelperDatabase\Helpers\Interfaces\IAmmunition;
use LaravelUploadHelper\NamePolicies\RandomNamePolicy;
use LaravelUploadHelper\UploadedFileImplementations\UploadedFileExtended;
use LaravelUploadHelper\UploadHelper\UploadHelper;
use LaravelUploadHelperImageTreatmentImplementations\FileTreatments\FixedSizeImageFileTreatment;

class UploadHelperDataTableAmmunition extends BaseAmmunition implements IAmmunition
{

    public function getAmmunition(UploadedFileExtended $file) : UploadHelper
    {
        $uploadHelper = $this->uploadHelperFactory->make($file);
        $uploadHelper->setNamingPolicy(new RandomNamePolicy());
        $uploadHelper->setFileTreatment(new FixedSizeImageFileTreatment(75,75));

        return $uploadHelper;

    }

}
