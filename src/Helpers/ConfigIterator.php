<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/23/19
 * Time: 12:12 AM
 */

namespace Azizyus\UploadHelperDatabase\Helpers;



use Azizyus\UploadHelperDatabase\Helpers\Interfaces\IteratorExtraCallable;
use Azizyus\UploadHelperDatabase\Models\GeneralImage;
use Azizyus\UploadHelperDatabase\Repositories\GeneralImageRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use LaravelUploadHelper\UploadHelper\UploadedFileCatcher;
use LaravelUploadHelper\UploadHelper\UploadHelperDeleter;

class ConfigIterator
{

    protected $generalImagesConfig;
    protected $tableEnum;
    protected $purposeEnums;
    protected $generalImageRepository;
    protected $callables;
    public function __construct(String $tableEnum, array $purposeEnums)
    {
        $this->generalImagesConfig = config("general-images");
        $this->tableEnum = $tableEnum;
        $this->purposeEnums = $purposeEnums;
        $this->generalImageRepository = new GeneralImageRepository();
        $this->callables = collect();
    }

    public function addCallable(IteratorExtraCallable $i,$key=null)
    {
        if($key=null) $key = uniqid();
        $this->callables->put($key,$i);
    }


    public function process(Model $model,Request $request)
    {

        $generalImageConfigArray = $this->generalImagesConfig;

        foreach ($generalImageConfigArray as $item)
        {

            if(in_array($item["enum"],$this->purposeEnums)) //that means; this model wants to create an image in this specifications
            {


                $inputName = $item["inputName"];
                $isImageUploaded = $request->exists($inputName);
                if($isImageUploaded)
                {

                    $file = UploadedFileCatcher::catchFile($inputName);


                    foreach ($item["variations"] as $variation)
                    {

                        $ammunitionFactory = new $variation["ammunition"];
                        $ammunitionInstance = $ammunitionFactory->getAmmunition($file);

                        $generalImage = $this->generalImageRepository
                            ->firstOrCreateGeneralImage($model->tableEnum(), $model->id,$variation["purposeEnum"]);

                        $uploadHelperDeleter = new UploadHelperDeleter();
                        $uploadHelperDeleter->delete($generalImage->fileName);

                        //your image should be save specified params to general_images table
                        $generalImage->fileName = $ammunitionInstance->saveFile();
                        $generalImage->save();

                    }

                    foreach ($this->callables as $c)
                    {
                        $c->call($model,$file);
                    }
                }

            }


        }

        $model->touch();

    }


}
