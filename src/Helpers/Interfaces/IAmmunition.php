<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/23/19
 * Time: 12:08 AM
 */

namespace Azizyus\UploadHelperDatabase\Helpers\Interfaces;


use LaravelUploadHelper\UploadedFileImplementations\UploadedFileExtended;
use LaravelUploadHelper\UploadHelper\UploadHelper;

interface IAmmunition
{
    public function getAmmunition(UploadedFileExtended $file) : UploadHelper;
}
