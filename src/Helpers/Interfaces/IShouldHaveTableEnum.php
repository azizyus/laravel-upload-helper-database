<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/23/19
 * Time: 12:05 AM
 */

namespace Azizyus\UploadHelperDatabase\Helpers\Interfaces;


interface IShouldHaveTableEnum
{

    public function tableEnum() : String;

}
