<?php


namespace Azizyus\UploadHelperDatabase\Helpers\Interfaces;


use Illuminate\Database\Eloquent\Model;
use LaravelUploadHelper\UploadedFileImplementations\UploadedFileExtended;

interface IteratorExtraCallable
{
    public function call(Model $model, UploadedFileExtended $file);
}
