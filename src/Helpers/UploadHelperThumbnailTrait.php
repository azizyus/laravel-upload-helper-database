<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/22/19
 * Time: 11:48 PM
 */

namespace Azizyus\UploadHelperDatabase\Helpers;


use Azizyus\UploadHelperDatabase\Models\GeneralImage;

trait UploadHelperThumbnailTrait
{

    protected $uploadHelperModelPrimaryKey = "id";

    public function dataTableImage()
    {
        return $this->hasOne(config("upload-helper-database.mainModel"),"modelEnum",$this->id)
            ->where("tableEnum",$this->tableEnum())
            ->where("purposeEnum",PurposeEnums::_DATATABLE);
    }

    public function thumbnailImageCropped()
    {
        return $this->hasOne(config("upload-helper-database.mainModel"),"modelEnum",$this->id)
            ->where("tableEnum",$this->tableEnum())
            ->where("purposeEnum",PurposeEnums::_THUMBNAIL);
    }

    public function thumbnailImageOriginal()
    {
        return $this->hasOne(config("upload-helper-database.mainModel"),"modelEnum",$this->id)
            ->where("tableEnum",$this->tableEnum())
            ->where("purposeEnum",PurposeEnums::_ORIGINAL_IMAGE);
    }


    public function getDataTablaImage()
    {
        if($this->dataTableImage)
        {
            return uploadHelperAsset($this->dataTableImage->fileName);
        }
        return null;
    }

    public function getThumbnailImage()
    {
        if($this->thumbnailImageCropped)
        {
            return uploadHelperAsset($this->thumbnailImageCropped->fileName);
        }
        return null;
    }

    public function getThumbnailImageOriginal()
    {
        if($this->thumbnailImageOriginal)
        {
            return uploadHelperAsset($this->thumbnailImageOriginal->fileName);
        }
        return null;
    }


}
