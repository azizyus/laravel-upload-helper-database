<?php


namespace Azizyus\UploadHelperDatabase\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ConfigIterationProcessForSingleImage
{

    public static function process(Model $model, String $enum, Request $request)
    {
        $configIterator = new ConfigIterator($model->tableEnum(),[
            $enum
        ]);

        $configIterator->process($model,$request);
    }

}
