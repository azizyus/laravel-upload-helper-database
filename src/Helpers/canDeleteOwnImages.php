<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 3/6/19
 * Time: 3:47 PM
 */

namespace Azizyus\UploadHelperDatabase\Helpers;


use Azizyus\UploadHelperDatabase\Repositories\GeneralImageRepository;

trait canDeleteOwnImages
{
    public function deleteAllImages()
    {
        $id = $this->id;
        $tableEnum = $this->tableEnum();

        $generalImageRepository = new GeneralImageRepository();
        $generalImageRepository->deleteSpecificGeneralImages($tableEnum,$id);


    }
}
