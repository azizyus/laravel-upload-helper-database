<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/23/19
 * Time: 12:22 AM
 */

namespace Azizyus\UploadHelperDatabase\Repositories;


use Azizyus\UploadHelperDatabase\Models\GeneralImage;
use LaravelUploadHelper\UploadHelper\UploadHelperDeleter;


class GeneralImageRepository
{

    public function baseQuery()
    {
        $mainModel = config("upload-helper-database.mainModel");
        return $mainModel::query();
    }


    public function firstOrCreateGeneralImage(String $tableEnum,Int $modelEnum,Int $purposeEnum)
    {
       $instance = $this->baseQuery()
            ->where("tableEnum",$tableEnum)
            ->where("modelEnum",$modelEnum)
            ->where("purposeEnum",$purposeEnum)
           ->first();


       if(!$instance)
       {
           $mainModel = config("upload-helper-database.mainModel");
           $instance = new $mainModel();
           $instance->tableEnum = $tableEnum;
           $instance->modelEnum = $modelEnum;
           $instance->purposeEnum = $purposeEnum;
       }



        return $instance;


    }

    # while deleting model you will probably delete all of it's images
    public function deleteSpecificGeneralImages(Int $tableEnum,Int $modelEnum)
    {

        $uploadHelperDeleter = new UploadHelperDeleter();
        $generalImages = $this->baseQuery()->where("tableEnum",$tableEnum)
            ->where("modelEnum",$modelEnum)->get();


        foreach ($generalImages as $generalImage)
        {

             $uploadHelperDeleter->delete($generalImage->fileName);

        }

    }

}
