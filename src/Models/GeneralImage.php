<?php

namespace Azizyus\UploadHelperDatabase\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralImage extends Model
{

    protected $fillable = [

        "fileName",
        "tableEnum"

    ];


}
