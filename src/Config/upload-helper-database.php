<?php


return [
    "uploadDir" => base_path("public/storage/file_uploads"),
    "uploadDirName" => "storage/file_uploads",
    "mainModel" => \Azizyus\UploadHelperDatabase\Models\GeneralImage::class
];
